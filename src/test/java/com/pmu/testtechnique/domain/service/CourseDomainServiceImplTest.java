package com.pmu.testtechnique.domain.service;


import com.pmu.testechnique.domaine.model.Course;
import com.pmu.testechnique.domaine.model.CoursePartant;
import com.pmu.testechnique.domaine.model.Partant;
import com.pmu.testechnique.domaine.service.impl.CourseDomainserviceImpl;
import com.pmu.testechnique.infrastructure.db.mapper.EntityMapper;
import com.pmu.testechnique.infrastructure.db.port.CoursePartantRepository;
import com.pmu.testechnique.infrastructure.db.port.CourseRepository;
import com.pmu.testechnique.infrastructure.db.port.PartantRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CourseDomainServiceImplTest {

    @Mock
    private CoursePartantRepository coursePartantRepository;

    @Mock
    private CourseRepository courseRepository;

    @Mock
    private PartantRepository partantRepository;

    @InjectMocks
    private CourseDomainserviceImpl courseService;

    @Test
    public void testAddCourse() {
        //given
        EntityMapper entityMapper = mock(EntityMapper.class);
        ReflectionTestUtils.setField(courseService, "entityMapper", entityMapper);
        Course course = new Course();
        Map<Partant, CoursePartant> coursePartants = new HashMap<>();
        Partant partant = new Partant();
        CoursePartant coursePartant = new CoursePartant();
        coursePartants.put(partant, coursePartant);
        com.pmu.testechnique.infrastructure.db.entity.Course courseEntity = new com.pmu.testechnique.infrastructure.db.entity.Course();
        com.pmu.testechnique.infrastructure.db.entity.Partant partantEntity = new com.pmu.testechnique.infrastructure.db.entity.Partant();
        when(partantRepository.findByNom(partantEntity.getNom())).thenReturn(Optional.empty());
        com.pmu.testechnique.infrastructure.db.entity.CoursePartant coursePartantEntity = new com.pmu.testechnique.infrastructure.db.entity.CoursePartant();
        when(entityMapper.toCourseEntity(course)).thenReturn(courseEntity);
        when(entityMapper.toPartantEntity(partant)).thenReturn(partantEntity);
        when(entityMapper.toCoursePartant(any(), any(),any())).thenReturn(coursePartantEntity);
        //when
        courseService.addCourse(course, coursePartants);
        //then
        verify(courseRepository, times(1)).save(any());
        verify(coursePartantRepository, times(1)).save(any());
        verify(partantRepository, times(1)).save(any());

    }
}
