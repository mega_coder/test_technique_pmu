package com.pmu.testechnique.application.validators;

import com.pmu.testechnique.application.dto.PartantDto;
import com.pmu.testechnique.application.validators.constraints.PartantConstraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.List;

public class PartantConstraintValidator implements ConstraintValidator<PartantConstraint, List<PartantDto>> {
    @Override
    public void initialize(PartantConstraint constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(List<PartantDto> partantDtos, ConstraintValidatorContext constraintValidatorContext) {
        for(int i= 0 ; i< partantDtos.size(); i++){
            if(!anyPartantMatch(i+1, partantDtos)) return false;
        }
        return true;
    }

    private Boolean anyPartantMatch(int numeroPartant, List<PartantDto> partantDtos){
        return partantDtos.stream().map(PartantDto::getNumeroPatant).anyMatch(numero -> numero == numeroPartant);
    }

}
