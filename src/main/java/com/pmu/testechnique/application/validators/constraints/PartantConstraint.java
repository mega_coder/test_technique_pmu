package com.pmu.testechnique.application.validators.constraints;

import com.pmu.testechnique.application.validators.PartantConstraintValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PartantConstraintValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PartantConstraint {
    String message() default "les numéros des partant ne sont pas bons";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}