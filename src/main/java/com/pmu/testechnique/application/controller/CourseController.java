package com.pmu.testechnique.application.controller;

import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.application.service.CourseService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CourseController {
    private final CourseService courseService;

    public CourseController(CourseService courseService) {
        this.courseService = courseService;
    }

    @PostMapping("/course")
    public ResponseEntity<String> addCourse(@RequestBody @Valid CourseDto courseDto){
        courseService.saveCourse(courseDto);
        return ResponseEntity.status(201).body("Course créée avec succees !!!");
    }

}
