package com.pmu.testechnique.application.mapper;


import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.application.dto.PartantDto;
import com.pmu.testechnique.domaine.model.Course;
import com.pmu.testechnique.domaine.model.CoursePartant;
import com.pmu.testechnique.domaine.model.Partant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface GeneralMapper {
    Course toCourse(CourseDto courseDto);
    Partant toPartant(PartantDto partantDto);
    @Mapping(source = "partantDto.numeroPatant" , target = "numeroPatant")
    CoursePartant toCoursePartant(Course course,Partant partant, PartantDto partantDto);
}
