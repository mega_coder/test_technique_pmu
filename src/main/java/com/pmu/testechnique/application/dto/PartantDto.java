package com.pmu.testechnique.application.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public class PartantDto {

    @NotBlank(message = "le nom du partant doit pas être vide")
    private String nom;

    @NotNull(message = "le numéro du partant doit pas être vide")
    private int numeroPatant;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumeroPatant() {
        return numeroPatant;
    }

    public void setNumeroPatant(int numeroPatant) {
        this.numeroPatant = numeroPatant;
    }

    public PartantDto(String nom, int numeroPatant) {
        this.nom = nom;
        this.numeroPatant = numeroPatant;
    }
}
