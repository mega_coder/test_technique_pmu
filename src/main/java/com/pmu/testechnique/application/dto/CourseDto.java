package com.pmu.testechnique.application.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pmu.testechnique.application.validators.constraints.PartantConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.List;

public class CourseDto {
    @NotBlank(message = "le nom de la course ne doit être vide")
    private String nom;
    @Size(min = 3)
    @PartantConstraint()
    @JsonProperty("partants")
    private List<@Valid PartantDto> partants;

    public List<PartantDto> getPartants() {
        return partants;
    }

    public void setPartants(List<PartantDto> partants) {
        this.partants = partants;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public CourseDto(String nom, List<@Valid PartantDto> partants) {
        this.nom = nom;
        this.partants = partants;
    }
}
