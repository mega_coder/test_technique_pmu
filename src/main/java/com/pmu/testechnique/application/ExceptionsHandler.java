package com.pmu.testechnique.application;


import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;



@RestControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<Object> handlexception(MethodArgumentNotValidException ex){
        BindingResult result = ex.getBindingResult();
        List<FieldError>  fieldErrors = result.getFieldErrors();
        List<String> messages = new ArrayList<>();
        for (FieldError fieldError : fieldErrors){
            messages.add(fieldError.getDefaultMessage());
        }
        return ResponseEntity.badRequest().body(messages);
    }
}
