package com.pmu.testechnique.application.service.impl;


import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.application.mapper.GeneralMapper;
import com.pmu.testechnique.application.service.CourseService;
import com.pmu.testechnique.domaine.model.Course;
import com.pmu.testechnique.domaine.model.CoursePartant;
import com.pmu.testechnique.domaine.model.Partant;
import com.pmu.testechnique.domaine.service.CourseDomainService;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    GeneralMapper generalMapper = Mappers.getMapper(GeneralMapper.class);
    private final CourseDomainService courseDomainService;

    public CourseServiceImpl(CourseDomainService courseDomainService) {
        this.courseDomainService = courseDomainService;
    }

    @Override
    public void saveCourse(CourseDto courseDto) {
        Course course = generalMapper.toCourse(courseDto);
        Map<Partant, CoursePartant>  partantCoursePartantMap = courseDto.getPartants()
                .stream().
                collect(Collectors.toMap(partantDto -> generalMapper.toPartant(partantDto),
                        partantDto -> generalMapper.toCoursePartant(course,  generalMapper.toPartant(partantDto), partantDto)));
        courseDomainService.addCourse(course, partantCoursePartantMap);
        //partie commenté car elle necessité la présente d'un serveur KAFKA
        //courseDomainService.produceCourse(courseDto);
    }
}
