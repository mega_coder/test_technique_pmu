package com.pmu.testechnique.application.service;

import com.pmu.testechnique.application.dto.CourseDto;

public interface CourseService {
    void saveCourse(CourseDto courseDto);
}
