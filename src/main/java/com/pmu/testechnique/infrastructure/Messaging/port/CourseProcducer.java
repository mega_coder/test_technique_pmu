package com.pmu.testechnique.infrastructure.Messaging.port;

import com.pmu.testechnique.infrastructure.Messaging.dto.CourseMessagingDto;

public interface CourseProcducer {
    void sendCourse(CourseMessagingDto courseMessagingDto);
}
