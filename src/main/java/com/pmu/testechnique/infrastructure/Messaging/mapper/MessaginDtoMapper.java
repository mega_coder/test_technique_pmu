package com.pmu.testechnique.infrastructure.Messaging.mapper;


import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.infrastructure.Messaging.dto.CourseMessagingDto;
import org.mapstruct.Mapper;

@Mapper
public interface MessaginDtoMapper {
    CourseMessagingDto toCourseMessagingDto(CourseDto courseDto);
}
