package com.pmu.testechnique.infrastructure.Messaging.adapter;

import com.pmu.testechnique.infrastructure.Messaging.dto.CourseMessagingDto;
import com.pmu.testechnique.infrastructure.Messaging.port.CourseProcducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Service
public class CourseProducerImpl implements CourseProcducer {

    @Value("${topic.name}")
    private String topicName;

    private final KafkaTemplate<String, CourseMessagingDto> kafkaTemplate;

    public CourseProducerImpl(KafkaTemplate<String, CourseMessagingDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void sendCourse(CourseMessagingDto courseMessagingDto) {
        kafkaTemplate.send(topicName, courseMessagingDto);
    }
}
