package com.pmu.testechnique.infrastructure.db.entity;


import jakarta.persistence.*;

import java.time.LocalDate;

@Entity
public class CoursePartant {
    @EmbeddedId
    CoursePartantKey id = new CoursePartantKey();
    @ManyToOne
    @MapsId("partantId")
    @JoinColumn(name = "partant_id")
    Partant partant;

    @ManyToOne
    @MapsId("courseId")
    @JoinColumn(name = "course_id")
    Course course;
    private int numeroPartant;


    LocalDate dateCourse;

    public LocalDate getDateCourse() {
        return dateCourse;
    }

    public void setDateCourse(LocalDate dateCourse) {
        this.dateCourse = dateCourse;
    }


    public CoursePartantKey getId() {
        return id;
    }

    public void setId(CoursePartantKey id) {
        this.id = id;
    }

    public Partant getPartant() {
        return partant;
    }

    public void setPartant(Partant partant) {
        this.partant = partant;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public int getNumeroPartant() {
        return numeroPartant;
    }

    public void setNumeroPartant(int numeroPartant) {
        this.numeroPartant = numeroPartant;
    }

    @Override
    public String toString() {
        return "CoursePartant{" +
                "id=" + id +
                ", partant=" + partant +
                ", course=" + course +
                ", numeroPartant=" + numeroPartant +
                ", dateCourse=" + dateCourse +
                '}';
    }
}
