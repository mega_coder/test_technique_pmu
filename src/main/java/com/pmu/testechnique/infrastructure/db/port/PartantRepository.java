package com.pmu.testechnique.infrastructure.db.port;

import com.pmu.testechnique.infrastructure.db.entity.Partant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PartantRepository extends JpaRepository<Partant, Integer> {
    Optional<Partant> findByNom(String nom);
}
