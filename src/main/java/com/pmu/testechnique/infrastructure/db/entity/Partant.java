package com.pmu.testechnique.infrastructure.db.entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name = "partant")
public class Partant{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nom;

    @OneToMany(mappedBy = "partant")
    List<CoursePartant> coursePartants;

    public List<CoursePartant> getCoursePartants() {
        return coursePartants;
    }

    public void setCoursePartants(List<CoursePartant> coursePartants) {
        this.coursePartants = coursePartants;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    @Override
    public String toString() {
        return "Partant{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", coursePartants=" + coursePartants +
                '}';
    }
}
