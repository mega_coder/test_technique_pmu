package com.pmu.testechnique.infrastructure.db.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class CoursePartantKey implements Serializable {
    @Column(name = "partant_id")
    Integer partantId;

    @Column(name = "course_id")
    Integer courseId;

    public Integer getPartantId() {
        return partantId;
    }

    public void setPartantId(Integer partantId) {
        this.partantId = partantId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "CoursePartantKey{" +
                "partantId=" + partantId +
                ", courseId=" + courseId +
                '}';
    }
}
