package com.pmu.testechnique.infrastructure.db.port;


import com.pmu.testechnique.infrastructure.db.entity.CoursePartant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursePartantRepository extends JpaRepository<CoursePartant, Integer> {
}
