package com.pmu.testechnique.infrastructure.db.entity;


import jakarta.persistence.*;

import java.util.List;
import java.util.UUID;

@Entity
@Table(name= "course")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String nom;
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(unique=true)
    private UUID numeroUnique;

    public UUID getNumeroUnique() {
        return numeroUnique;
    }

    public void setNumeroUnique(UUID numeroUnique) {
        this.numeroUnique = numeroUnique;
    }

    @OneToMany(mappedBy = "course")
    List<CoursePartant> coursePartants;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<CoursePartant> getCoursePartants() {
        return coursePartants;
    }

    public void setCoursePartants(List<CoursePartant> coursePartants) {
        this.coursePartants = coursePartants;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", numeroUnique=" + numeroUnique +
                ", coursePartants=" + coursePartants +
                '}';
    }
}
