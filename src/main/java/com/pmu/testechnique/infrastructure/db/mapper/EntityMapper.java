package com.pmu.testechnique.infrastructure.db.mapper;

import com.pmu.testechnique.infrastructure.db.entity.Course;
import com.pmu.testechnique.infrastructure.db.entity.CoursePartant;
import com.pmu.testechnique.infrastructure.db.entity.Partant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface EntityMapper {
    @Mapping(target = "numeroUnique" , expression = "java(java.util.UUID.randomUUID())")
    @Mapping(target = "coursePartants" , ignore = true)
    Course toCourseEntity(com.pmu.testechnique.domaine.model.Course course);

    @Mapping(target = "coursePartants" , ignore = true)
    Partant toPartantEntity(com.pmu.testechnique.domaine.model.Partant partant);

    @Mapping(target = "id.partantId", source = "partant.id")
    @Mapping(target = "id.courseId", source = "course.id")
    CoursePartant toCoursePartant(com.pmu.testechnique.domaine.model.CoursePartant coursePartant, Course course, Partant partant);
}
