package com.pmu.testechnique.domaine.service;

import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.domaine.model.Course;
import com.pmu.testechnique.domaine.model.CoursePartant;
import com.pmu.testechnique.domaine.model.Partant;
import java.util.Map;

public interface CourseDomainService {
    /**
     * sauvegarder une course dans la base de données
     * @param course
     * @param coursePartants
     */
    void addCourse(Course course, Map<Partant, CoursePartant> coursePartants);

    /**
     * envoyer une course au topic kafka courses
     * @param courseDto
     */
    void produceCourse(CourseDto courseDto);
}
