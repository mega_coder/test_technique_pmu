package com.pmu.testechnique.domaine.service.impl;

import com.pmu.testechnique.application.dto.CourseDto;
import com.pmu.testechnique.domaine.model.Course;
import com.pmu.testechnique.domaine.model.CoursePartant;
import com.pmu.testechnique.domaine.model.Partant;
import com.pmu.testechnique.domaine.service.CourseDomainService;
import com.pmu.testechnique.infrastructure.Messaging.mapper.MessaginDtoMapper;
import com.pmu.testechnique.infrastructure.Messaging.port.CourseProcducer;
import com.pmu.testechnique.infrastructure.db.mapper.EntityMapper;
import com.pmu.testechnique.infrastructure.db.port.CoursePartantRepository;
import com.pmu.testechnique.infrastructure.db.port.CourseRepository;
import com.pmu.testechnique.infrastructure.db.port.PartantRepository;
import org.apache.kafka.clients.producer.Producer;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;


@Service
public class CourseDomainserviceImpl implements CourseDomainService {

    private final CourseRepository courseRepository;
    private final PartantRepository partantRepository;
    private final CoursePartantRepository coursePartantRepository;
    private EntityMapper entityMapper = Mappers.getMapper(EntityMapper.class);
    private MessaginDtoMapper messaginDtoMapper = Mappers.getMapper(MessaginDtoMapper.class);
    private final CourseProcducer courseProducer ;


    public CourseDomainserviceImpl(CourseRepository courseRepository, PartantRepository partantRepository, CoursePartantRepository coursePartantRepository, CourseProcducer courseProducer) {
        this.courseRepository = courseRepository;
        this.partantRepository = partantRepository;
        this.coursePartantRepository = coursePartantRepository;
        this.courseProducer = courseProducer;
    }

    @Override
    public void addCourse(Course course, Map<Partant, CoursePartant> coursePartants) {
        com.pmu.testechnique.infrastructure.db.entity.Course courseEntity = entityMapper.toCourseEntity(course);
        courseRepository.save(courseEntity);
        for (Map.Entry<Partant, CoursePartant> entry : coursePartants.entrySet()) {
            Optional<com.pmu.testechnique.infrastructure.db.entity.Partant> partantOptional = partantRepository.findByNom(entry.getKey().getNom());
            if(!partantOptional.isPresent()) {
                partantRepository.save(entityMapper.toPartantEntity(entry.getKey()));
            }
            com.pmu.testechnique.infrastructure.db.entity.CoursePartant coursePartant = entityMapper.toCoursePartant(entry.getValue(), courseEntity, entityMapper.toPartantEntity(entry.getKey()));
            coursePartant = coursePartantRepository.save(coursePartant);
        }
    }

    @Override
    public void produceCourse(CourseDto courseDto) {
        courseProducer.sendCourse(messaginDtoMapper.toCourseMessagingDto(courseDto));
    }
}
