package com.pmu.testechnique.domaine.model;


public class CoursePartant {
    private Course course;
    private Partant partant;
    private int numeroPatant;


    public int getNumeroPatant() {
        return numeroPatant;
    }

    public void setNumeroPatant(int numeroPatant) {
        this.numeroPatant = numeroPatant;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Partant getPartant() {
        return partant;
    }

    public void setPartant(Partant partant) {
        this.partant = partant;
    }

    @Override
    public String toString() {
        return "CoursePartant{" +
                "course=" + course +
                ", partant=" + partant +
                ", numeroPatant=" + numeroPatant +
                '}';
    }
}
