package com.pmu.testechnique.domaine.model;

public class Course {
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Course{" +
                "nom='" + nom + '\'' +
                '}';
    }
}
