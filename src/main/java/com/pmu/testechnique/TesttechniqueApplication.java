package com.pmu.testechnique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class TesttechniqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesttechniqueApplication.class, args);
	}

}
